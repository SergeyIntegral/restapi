﻿using System;
using System.Collections.Generic;

namespace Cross.IoC
{
    public static class AutoMapperConfiguration
    {
        public static IEnumerable<Type> GetAutoMapperProfiles()
        {
            var result = new List<Type>();
            result.Add(typeof(Cross.AppService.Mappings.Mapping));
            return result;
        }
    }
}