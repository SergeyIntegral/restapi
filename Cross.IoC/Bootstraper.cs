﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

namespace Cross.IoC
{
    public static class Bootstraper
    {
        public static void Configure(IServiceCollection services)
        {
            Configure(services, Cross.DAL.IoC.Module.GetTypes());
            Configure(services, Cross.DomainService.IoC.Module.GetTypes());
            Configure(services, Cross.AppService.IoC.Module.GetTypes());
        }

        private static void Configure(IServiceCollection services, Dictionary<Type, Type> types)
        {
            foreach (var type in types)
                services.AddScoped(type.Key, type.Value);
        }
    }
}