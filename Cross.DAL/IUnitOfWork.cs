﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Cross.DAL
{
    public interface IUnitOfWork
    {
        DbContext Context { get; }

        void SaveChanges();

        Task SaveChangesAsync();
    }
}