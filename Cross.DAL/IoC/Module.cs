﻿using System;
using System.Collections.Generic;

namespace Cross.DAL.IoC
{
    public static class Module
    {
        public static Dictionary<Type, Type> GetTypes()
        {
            var dic = new Dictionary<Type, Type>();
            dic.Add(typeof(IUnitOfWork), typeof(UnitOfWork));
            dic.Add(typeof(IExerciseObjectRepository), typeof(ExerciseObjectRepository));
            dic.Add(typeof(IExerciseDictionaryObjectRepository), typeof(ExerciseDictionaryRepository));
            dic.Add(typeof(IExerciseSettingRepository), typeof(ExerciseSettingRepository));
            dic.Add(typeof(IRoundObjectRepository), typeof(RoundObjectRepository));
            dic.Add(typeof(IWodObjectRepository), typeof(WodObjectRepository));
            return dic;
        }
    }
}