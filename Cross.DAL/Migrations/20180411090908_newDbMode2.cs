﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Cross.DAL.Migrations
{
    public partial class newDbMode2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exercise_ExerciseDictionary_ExerciseDictionaryId",
                table: "Exercise");

            migrationBuilder.AlterColumn<int>(
                name: "ExerciseDictionaryId",
                table: "Exercise",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Exercise_ExerciseDictionary_ExerciseDictionaryId",
                table: "Exercise",
                column: "ExerciseDictionaryId",
                principalTable: "ExerciseDictionary",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exercise_ExerciseDictionary_ExerciseDictionaryId",
                table: "Exercise");

            migrationBuilder.AlterColumn<int>(
                name: "ExerciseDictionaryId",
                table: "Exercise",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Exercise_ExerciseDictionary_ExerciseDictionaryId",
                table: "Exercise",
                column: "ExerciseDictionaryId",
                principalTable: "ExerciseDictionary",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
