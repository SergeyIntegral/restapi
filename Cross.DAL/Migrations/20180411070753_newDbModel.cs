﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Cross.DAL.Migrations
{
    public partial class newDbModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExerciseDictionary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExerciseName = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseDictionary", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Wod",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpectedTime = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ResultTime = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wod", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseExtendedSettings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExerciseDictionaryId = table.Column<int>(nullable: false),
                    Measure = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseExtendedSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseExtendedSettings_ExerciseDictionary_ExerciseDictionaryId",
                        column: x => x.ExerciseDictionaryId,
                        principalTable: "ExerciseDictionary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Round",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WodId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Round", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Round_Wod_WodId",
                        column: x => x.WodId,
                        principalTable: "Wod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Exercise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExerciseDictionaryId = table.Column<int>(nullable: false),
                    ExpectedCount = table.Column<int>(nullable: false),
                    ExpectedWeight = table.Column<int>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    ResultCount = table.Column<int>(nullable: false),
                    ResultWeight = table.Column<int>(nullable: true),
                    RoundId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exercise_ExerciseDictionary_ExerciseDictionaryId",
                        column: x => x.ExerciseDictionaryId,
                        principalTable: "ExerciseDictionary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Exercise_Round_RoundId",
                        column: x => x.RoundId,
                        principalTable: "Round",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Exercise_ExerciseDictionaryId",
                table: "Exercise",
                column: "ExerciseDictionaryId");

            migrationBuilder.CreateIndex(
                name: "IX_Exercise_RoundId",
                table: "Exercise",
                column: "RoundId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseExtendedSettings_ExerciseDictionaryId",
                table: "ExerciseExtendedSettings",
                column: "ExerciseDictionaryId");

            migrationBuilder.CreateIndex(
                name: "IX_Round_WodId",
                table: "Round",
                column: "WodId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Exercise");

            migrationBuilder.DropTable(
                name: "ExerciseExtendedSettings");

            migrationBuilder.DropTable(
                name: "Round");

            migrationBuilder.DropTable(
                name: "ExerciseDictionary");

            migrationBuilder.DropTable(
                name: "Wod");
        }
    }
}
