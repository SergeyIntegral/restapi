﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Cross.DAL.Migrations
{
    public partial class startData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"SET IDENTITY_INSERT [dbo].[ExerciseDictionary] ON

INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (1, N'Body weight', NULL)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (2, N'Air Squat', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (3, N'Pull-up', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (4, N'Push-up', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (5, N'Chest-To-Bar', 4)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (6, N'L-Sit', 4)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (7, N'Sit-up', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (8, N'Jump', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (9, N'Jump over bar', 8)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (10, N'Jump on box', 8)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (11, N'Run', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (12, N'Bar Muscle Up', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (13, N'Burpee', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (14, N'Hand Stand Push Up', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (15, N'Hand Walk', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (16, N'Toes To Bar', 1)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (17, N'Barbell, dumbbell, kettlebell', NULL)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (18, N'Thruster', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (19, N'Snatch', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (20, N'Snatch', 19)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (21, N'Hang Power Snatch', 19)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (22, N'Clean', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (23, N'Power Clean', 22)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (24, N'Hang Power Clean', 22)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (25, N'Push', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (26, N'Push Press', 4)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (27, N'Push Jerk', 4)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (28, N'Front squat', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (29, N'Lunge', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (30, N'Overhead', 29)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (31, N'Dumbbell, kettlebell', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (32, N'Swing', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (33, N'Med Ball', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (34, N'Wall Ball', 33)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (35, N'Thruster', 33)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (36, N'Clean', 33)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (37, N'Rings', 17)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (38, N'Strict Muscle-up', 37)
INSERT [dbo].[ExerciseDictionary] ([Id],  [ExerciseName], [ParentId]) VALUES (39, N'Ring Dip', 37)
SET IDENTITY_INSERT [dbo].[ExerciseDictionary] OFF");

            migrationBuilder.Sql(@"SET IDENTITY_INSERT [dbo].[ExerciseExtendedSettings] ON

INSERT [dbo].[ExerciseExtendedSettings] ([Id], [ExerciseDictionaryId], [Measure], [Value]) VALUES (1, 33, N'кг', N'24')
INSERT [dbo].[ExerciseExtendedSettings] ([Id], [ExerciseDictionaryId], [Measure], [Value]) VALUES (2, 33, N'кг', N'9')
INSERT [dbo].[ExerciseExtendedSettings] ([Id], [ExerciseDictionaryId], [Measure], [Value]) VALUES (3, 33, N'кг', N'6')
INSERT [dbo].[ExerciseExtendedSettings] ([Id], [ExerciseDictionaryId], [Measure], [Value]) VALUES (4, 33, N'кг', N'3')
SET IDENTITY_INSERT [dbo].[ExerciseExtendedSettings] OFF");


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}