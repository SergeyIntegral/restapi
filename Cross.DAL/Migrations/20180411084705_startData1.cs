﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Cross.DAL.Migrations
{
    public partial class startData1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exercise_Round_RoundId",
                table: "Exercise");

            migrationBuilder.DropForeignKey(
                name: "FK_Round_Wod_WodId",
                table: "Round");

            migrationBuilder.AlterColumn<int>(
                name: "WodId",
                table: "Round",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RoundId",
                table: "Exercise",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ResultCount",
                table: "Exercise",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Order",
                table: "Exercise",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ExpectedCount",
                table: "Exercise",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Exercise_Round_RoundId",
                table: "Exercise",
                column: "RoundId",
                principalTable: "Round",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Round_Wod_WodId",
                table: "Round",
                column: "WodId",
                principalTable: "Wod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exercise_Round_RoundId",
                table: "Exercise");

            migrationBuilder.DropForeignKey(
                name: "FK_Round_Wod_WodId",
                table: "Round");

            migrationBuilder.AlterColumn<int>(
                name: "WodId",
                table: "Round",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RoundId",
                table: "Exercise",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ResultCount",
                table: "Exercise",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Order",
                table: "Exercise",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ExpectedCount",
                table: "Exercise",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Exercise_Round_RoundId",
                table: "Exercise",
                column: "RoundId",
                principalTable: "Round",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Round_Wod_WodId",
                table: "Round",
                column: "WodId",
                principalTable: "Wod",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
