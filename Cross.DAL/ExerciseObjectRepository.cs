﻿using Cross.DAL.Repositories.Abstract;
using Cross.DAL.Repositories.Base;
using Cross.Model;

namespace Cross.DAL
{
    /// <summary>
    /// Упражнения
    /// </summary>
    public interface IExerciseObjectRepository : IIntRepository<Exercise>
    {
    }

    public class ExerciseObjectRepository : IntRepository<Exercise>, IExerciseObjectRepository
    {
        public ExerciseObjectRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }


    /// <summary>
    /// Справочник упражнений
    /// </summary>
    public interface IExerciseDictionaryObjectRepository : IIntRepository<ExerciseDictionary>
    {
    }

    public class ExerciseDictionaryRepository : IntRepository<ExerciseDictionary>, IExerciseDictionaryObjectRepository
    {
        public ExerciseDictionaryRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }

    public interface IExerciseSettingRepository : IIntRepository<ExerciseExtendedSettings>
    {
    }


    /// <summary>
    /// Доп информация по упражнениям
    /// </summary>
    public class ExerciseSettingRepository : IntRepository<ExerciseExtendedSettings>, IExerciseSettingRepository
    {
        public ExerciseSettingRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }

}