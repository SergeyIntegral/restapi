﻿using Cross.DAL.Repositories.Abstract;
using Cross.DAL.Repositories.Base;
using Cross.Model;

namespace Cross.DAL
{
    /// <summary>
    /// Воды
    /// </summary>
    public interface IWodObjectRepository : IIntRepository<Wod>
    {
    }

    public class WodObjectRepository : IntRepository<Wod>, IWodObjectRepository
    {
        public WodObjectRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }
}