﻿using Cross.DAL.Repositories.Abstract;
using Cross.DAL.Repositories.Base;
using Cross.Model;

namespace Cross.DAL
{
    /// <summary>
    /// Раунды
    /// </summary>
    public interface IRoundObjectRepository : IIntRepository<Round>
    {
    }

    public class RoundObjectRepository : IntRepository<Round>, IRoundObjectRepository
    {
        public RoundObjectRepository(IUnitOfWork uow) : base(uow)
        {
        }
    }
}