﻿using System;
using System.Threading.Tasks;
using Cross.Model;
using Cross.Model.Abstract;
using Microsoft.EntityFrameworkCore;


namespace Cross.DAL
{
    public class AppDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {

        }


        public override int SaveChanges()
        {
            setDatesProperies();
            return base.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            setDatesProperies();
            return base.SaveChangesAsync();
        }

        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseExtendedSettings> ExtendedSettingses { get; set; }
        public DbSet<Round> Rounds { get; set; }
        public DbSet<Wod> Wods { get; set; }

        private void setDatesProperies()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                var entity = entry.Entity as IDatesEntity;
                if (entity != null)
                {
                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedDate = DateTime.UtcNow;
                        entity.ModifiedDate = DateTime.UtcNow;
                    }
                    else if (entry.State == EntityState.Modified)
                    {
                        entity.ModifiedDate = DateTime.UtcNow;
                    }
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>().ToTable("Exercise");
            modelBuilder.Entity<ExerciseExtendedSettings>().ToTable("ExerciseExtendedSettings");
            modelBuilder.Entity<Round>().ToTable("Round");
            modelBuilder.Entity<Wod>().ToTable("Wod");
        }
    }
}