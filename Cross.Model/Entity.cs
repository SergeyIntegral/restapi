﻿using Cross.Model.Abstract;

namespace Cross.Model
{
    public class IntEntity : IIntEntity
    {
        public int Id { get; set; }
    }
}