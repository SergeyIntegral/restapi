﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Cross.Model
{
    public class ExerciseExtendedSettings : IntEntity
    {

        public int ExerciseDictionaryId { get; set; }

        [ForeignKey("ExerciseDictionaryId")]
        public virtual ExerciseDictionary ExerciseDictionary { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public string Measure { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }
    }
}