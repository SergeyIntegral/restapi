﻿using System;
using Cross.Model.Abstract;

namespace Cross.Model
{
    /// <summary>
    /// Упражнение
    /// </summary>
    public class ExerciseDictionary : IntEntity
    {
        public int? ParentId { get; set; }
        public string ExerciseName { get; set; }

    }
}