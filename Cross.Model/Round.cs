﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cross.Model
{
    public class Round : IntEntity
    {
        public Round()
        {

        }

        public Round(int wodId)
        {
            this.WodId = wodId;
        }

        public virtual ICollection<Exercise> Exercise { get; set; }

        public int? WodId { get; set; }

        [ForeignKey("WodId")]
        public virtual Wod Wod{get; set; }

    }
}