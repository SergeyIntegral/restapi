﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cross.Model.Abstract;

namespace Cross.Model
{
    public class Exercise : IntEntity, IDatesEntity
    {
        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Планируемое кол-во повторений
        /// </summary>
        public int? ExpectedCount { get; set; }

        /// <summary>
        /// Кол-во повторений по факту
        /// </summary>
        public int? ResultCount { get; set; }

        /// <summary>
        /// Вес планируемый
        /// </summary>
        public int? ExpectedWeight { get; set; }

        /// <summary>
        /// Вес по факту
        /// </summary>
        public int? ResultWeight { get; set; }

        /// <summary>
        /// Порядок
        /// </summary>
        public int? Order { get; set; }

        public int? ExerciseDictionaryId { get; set; }

        [ForeignKey("ExerciseDictionaryId")]
        public virtual ExerciseDictionary ExerciseDictionary { get; set; }

        public int? RoundId { get; set; }

        [ForeignKey("RoundId")]
        public virtual Round Round { get; set; }

    }
}