﻿using System;
using System.Collections.Generic;
using Cross.Model.Abstract;

namespace Cross.Model
{
    public class Wod : IntEntity, IDatesEntity
    {
        public Wod()
        {
            this.CreatedDate = DateTime.UtcNow;
        }

        public virtual ICollection<Round> Rounds { get; set; }

        /// <summary>
        /// Ожидаемое время
        /// </summary>
        public string ExpectedTime { get; set; }

        /// <summary>
        /// Реальное время
        /// </summary>
        public string ResultTime { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}