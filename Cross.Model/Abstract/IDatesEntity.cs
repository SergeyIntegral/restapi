﻿using System;

namespace Cross.Model.Abstract
{
    public interface IDatesEntity
    {
        DateTime CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
    }
}