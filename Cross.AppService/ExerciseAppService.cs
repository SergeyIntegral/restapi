﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using Cross.AppService.DTOs;
using Cross.AppService.Extensions;
using Cross.AppService.Interfaces;
using Cross.DomainService.IServices;
using Cross.Model;

namespace Cross.AppService
{
    public class ExerciseAppService : IExerciseAppService
    {
        private readonly IExerciseDictionaryService _exerciseDomainService;

        public ExerciseAppService(IExerciseDictionaryService exerciseDomainService)
        {
            _exerciseDomainService = exerciseDomainService;
        }
        //TODO проверить асинхронные методы!
        public void Delete(int id)
        {
            _exerciseDomainService.Delete(id);
        }

        public void Delete(ExerciseDictionaryDTO entity)
        {
            _exerciseDomainService.Delete(entity.MapTo<ExerciseDictionary>());
        }

        public ExerciseDictionaryDTO FindById(int id)
        {
            return _exerciseDomainService.FindById(id).MapTo<ExerciseDictionaryDTO>();
        }

        public async Task<ExerciseDictionaryDTO> FindByIdAsync(int id)
        {
            var result = await _exerciseDomainService.FindByIdAsync(id);
            return result.MapTo<ExerciseDictionaryDTO>();
        }

        public IEnumerable<ExerciseDictionaryDTO> FindByIds(IEnumerable<int> ids)
        {
            return _exerciseDomainService.FindByIds(ids).EnumerableTo<ExerciseDictionaryDTO>();
        }

        public async Task<IEnumerable<ExerciseDictionaryDTO>> FindByIdsAsync(IEnumerable<int> ids)
        {
            var result = await _exerciseDomainService.FindByIdsAsync(ids);
            return result.EnumerableTo<ExerciseDictionaryDTO>();
        }

        public IEnumerable<ExerciseDictionaryDTO> GetAll()
        {
            return _exerciseDomainService.GetAll().EnumerableTo<ExerciseDictionaryDTO>();
        }

        public async Task<IEnumerable<ExerciseDictionaryDTO>> GetAllAsync()
        {
            var result = await _exerciseDomainService.GetAllAsync();
            return result.EnumerableTo<ExerciseDictionaryDTO>();
        }

        public ExerciseDictionaryDTO Insert(ExerciseDictionaryDTO entity)
        {
            return _exerciseDomainService.Insert(entity.MapTo<ExerciseDictionary>()).MapTo<ExerciseDictionaryDTO>();
        }

        public ExerciseDictionaryDTO Update(ExerciseDictionaryDTO entity)
        {
            return _exerciseDomainService.Update(entity.MapTo<ExerciseDictionary>()).MapTo<ExerciseDictionaryDTO>();
        }

        public async Task<IEnumerable<ExerciseDictionaryDTO>> GetRootMembers()
        {
            var result = await _exerciseDomainService.GetRootMembers();
            return result.EnumerableTo<ExerciseDictionaryDTO>();
        }

        public async Task<IEnumerable<ExerciseDictionaryDTO>> GetMembersByParentId(int parentId)
        {
            var result = await _exerciseDomainService.GetMembersByParentId(parentId);
            return result.EnumerableTo<ExerciseDictionaryDTO>();
        }


        public async Task<IEnumerable<ExerciseDictionaryWithSettingsDTO>> GetRootsWithSettings()
        {

            var exercises = await _exerciseDomainService.GetRootMembers();

            var settings = await _exerciseDomainService.GetExerciseDictionarySettings();

            var result = exercises.GroupJoin(settings, e => e.Id, o => o.ExerciseDictionaryId, (e, o) => new ExerciseDictionaryWithSettingsDTO
            {
                ExerciseName = e.ExerciseName,
                Id = e.Id,
                ParentId = e.ParentId,
                ExerciseSettings = Mapper.Map<List<ExerciseDictionarySettingsDTO>>(o.ToList())
            });
            return result;
        }


        public async Task<IEnumerable<ExerciseDictionaryWithSettingsDTO>> GetMembersByParentIdWithSettings(int parentId)
        {
            var exercises = await _exerciseDomainService.GetMembersByParentId(parentId);

            var settings = await GetSettings(parentId);

            var result =  exercises.Select(e => new ExerciseDictionaryWithSettingsDTO()
            {
                ExerciseName = e.ExerciseName,
                Id = e.Id,
                ParentId = e.ParentId,
                ExerciseSettings = settings.AsEnumerable().EnumerableTo<ExerciseDictionarySettingsDTO>().ToList()

            });
            return result;
        }

        public async Task<IEnumerable<ExerciseDictionarySettingsDTO>> GetSettings(int exerciseDictId)
        {
            var res = await _exerciseDomainService.GetExerciseDictionarySettingsByExerciseDictionaryId(exerciseDictId);
            return res.AsEnumerable().EnumerableTo<ExerciseDictionarySettingsDTO>();
        }

        public async Task<IEnumerable<ExerciseDTO>> GetAllExercisesByRoundId(int roundId)
        {
            var result = await _exerciseDomainService.GetAllExercisesByRoundId(roundId);
            return result.AsEnumerable().EnumerableTo<ExerciseDTO>();
        }

        public ExerciseDTO InsertNewExerciseWithRoundId(int roundId)
        {
            return _exerciseDomainService.InsertNewExerciseWithRoundId(roundId).MapTo<ExerciseDTO>();
        }

        public async Task<ExerciseDTO> AddExerciseInsertNewExerciseWithRoundIdAsync(int roundId)
        {
            var result = await _exerciseDomainService.InsertNewExerciseWithRoundIdAsync(roundId);
            return result.MapTo<ExerciseDTO>();
        }


        public async Task<ExerciseDTO> GetById(int id)
        {
            var result = await _exerciseDomainService.GetById(id);
            return result.MapTo<ExerciseDTO>();
        }
    }
}