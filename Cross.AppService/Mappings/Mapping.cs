﻿using Cross.Model;

namespace Cross.AppService.Mappings
{
    public class Mapping : AutoMapper.Profile
    {
        public Mapping()
        {
            CreateMap<DTOs.ExerciseDictionaryDTO, ExerciseDictionary>().ReverseMap();
            CreateMap<DTOs.ExerciseDictionarySettingsDTO, ExerciseExtendedSettings>().ReverseMap();
            CreateMap<DTOs.WodDTO, Wod>().ReverseMap();
            CreateMap<DTOs.RoundDTO, Round>().ReverseMap();
            CreateMap<DTOs.ExerciseDTO, Exercise>().ReverseMap();
        }
    }
}