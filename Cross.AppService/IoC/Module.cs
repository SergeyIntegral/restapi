﻿using System;
using System.Collections.Generic;
using Cross.AppService.Interfaces;

namespace Cross.AppService.IoC
{
    public static class Module
    {
        public static Dictionary<Type, Type> GetTypes()
        {
            var dic = new Dictionary<Type, Type>();
            dic.Add(typeof(IExerciseAppService), typeof(ExerciseAppService));
            dic.Add(typeof(IRoundAppService), typeof(RoundAppService));
            dic.Add(typeof(IWodAppService), typeof(WodAppService));
            return dic;
        }
    }
}