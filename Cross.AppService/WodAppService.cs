﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cross.AppService.DTOs;
using Cross.AppService.Extensions;
using Cross.AppService.Interfaces;
using Cross.DomainService.IServices;

namespace Cross.AppService
{
    public class WodAppService : IWodAppService
    {
        private readonly IWodService _wodService;

        public WodAppService(IWodService wodService)
        {
            _wodService = wodService;
        }

        public WodDTO CreateNewWod()
        {
            var res =_wodService.CreateNewWod().MapTo<WodDTO>();
            return res;
        }

        public async Task<IEnumerable<WodDTO>> GetAllWods()
        {
            var allWods = await _wodService.GetAllWods();

            return allWods.AsEnumerable().EnumerableTo<WodDTO>();
        }
    }
}