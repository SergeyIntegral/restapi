﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cross.AppService.DTOs;
using Cross.AppService.Extensions;
using Cross.AppService.Interfaces;
using Cross.DomainService.Extensions;
using Cross.DomainService.IServices;
using Cross.Model;

namespace Cross.AppService
{
    public class RoundAppService : IRoundAppService
    {
        private readonly IRoundService _roundService;
        private readonly IExerciseAppService _exerciseSerivce;

        public RoundAppService(IRoundService roundService, IExerciseAppService exerciseSerivce)
        {
            _roundService = roundService;
            _exerciseSerivce = exerciseSerivce;
        }


        public RoundDTO AddNewRound(int wodId)
        {
            return _roundService.AddNewRound(wodId).MapTo<RoundDTO>();
        }

        public async Task<IEnumerable<RoundDTO>> GetRoundsByWodId(int wodId)
        {
            var data = await _roundService.GetRoundsByWodId(wodId);

            Dictionary<int, ExerciseDictionaryDTO> dict = new Dictionary<int, ExerciseDictionaryDTO>();

            await data.Where(r => r.Exercise != null && r.Exercise.Any()).Select(w=>w.Exercise).ToList().ForEachAsync(async e =>
            {
                await e.Where(x=>x.ExerciseDictionaryId.HasValue).ToList().ForEachAsync(async r =>
                {
                    var exercise = r;
                    ExerciseDictionaryDTO exerciseDictionaryDto =
                        _exerciseSerivce.FindById(exercise.ExerciseDictionaryId.Value);

                    if (exerciseDictionaryDto != null)
                    {
                        dict.Add(exercise.Id, exerciseDictionaryDto);
                    }
                });
            });

            var Mappedresult = data.AsEnumerable().EnumerableTo<RoundDTO>().ToList();

            Mappedresult.ForEach(x =>
            {
                if (x.Exercise != null && x.Exercise.Any())
                {
                    for (var index = 0; index < x.Exercise.Count; index++)
                    {
                        var r = x.Exercise[index];
                        r.ExerciseName = dict.Any(e => e.Key == r.Id) ? dict[r.Id]?.ExerciseName : "";
                    }
                }
            });
            return Mappedresult;
        }

        public async Task<RoundDTO> CopyRound(int roundId)
        {
            var result = await _roundService.CopyRound(roundId);
            return result.MapTo<RoundDTO>();
        }
    }
}