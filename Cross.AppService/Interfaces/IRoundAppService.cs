﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cross.AppService.DTOs;

namespace Cross.AppService.Interfaces
{
    public interface IRoundAppService
    {
        RoundDTO AddNewRound(int wodId);

        Task <IEnumerable<RoundDTO>> GetRoundsByWodId(int wodId);

        Task<RoundDTO> CopyRound(int roundId);
    }
}