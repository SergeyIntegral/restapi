﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cross.AppService.DTOs;
using Cross.Model;

namespace Cross.AppService.Interfaces
{
    public interface IExerciseAppService
    {
        void Delete(int id);

        void Delete(ExerciseDictionaryDTO entity);

        ExerciseDictionaryDTO FindById(int id);

        Task<ExerciseDictionaryDTO> FindByIdAsync(int id);

        IEnumerable<ExerciseDictionaryDTO> FindByIds(IEnumerable<int> ids);

        Task<IEnumerable<ExerciseDictionaryDTO>> FindByIdsAsync(IEnumerable<int> ids);

        IEnumerable<ExerciseDictionaryDTO> GetAll();

        Task<IEnumerable<ExerciseDictionaryDTO>> GetAllAsync();

        ExerciseDictionaryDTO Insert(ExerciseDictionaryDTO entity);

        ExerciseDictionaryDTO Update(ExerciseDictionaryDTO entity);

        Task<IEnumerable<ExerciseDictionaryDTO>> GetRootMembers();

        Task<IEnumerable<ExerciseDictionaryDTO>> GetMembersByParentId(int parentId);

        Task<IEnumerable<ExerciseDictionaryWithSettingsDTO>> GetRootsWithSettings();

        Task<IEnumerable<ExerciseDictionaryWithSettingsDTO>> GetMembersByParentIdWithSettings(int parentId);

        Task<IEnumerable<ExerciseDTO>> GetAllExercisesByRoundId(int roundId);

        ExerciseDTO InsertNewExerciseWithRoundId(int roundId);

        Task<IEnumerable<ExerciseDictionarySettingsDTO>> GetSettings(int exerciseDictId);

        Task<ExerciseDTO> AddExerciseInsertNewExerciseWithRoundIdAsync(int roundId);

        Task<ExerciseDTO> GetById(int id);

    }
}