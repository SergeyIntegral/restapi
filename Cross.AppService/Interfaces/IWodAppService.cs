﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cross.AppService.DTOs;

namespace Cross.AppService.Interfaces
{
    public interface IWodAppService
    {
        WodDTO CreateNewWod();

        Task <IEnumerable<WodDTO>> GetAllWods();
    }
}