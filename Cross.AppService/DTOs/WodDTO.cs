﻿using System;
using System.Collections.Generic;

namespace Cross.AppService.DTOs
{
    public class WodDTO
    {
        public WodDTO()
        {
            this.CreatedDate = DateTime.UtcNow;
            this.Rounds = new List<RoundDTO>();
        }

        public int Id { get; set; }

        public string ExpectedTime { get; set; }

        public string ResultTime { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public List<RoundDTO> Rounds { get; set; }
    }
}