﻿using System;
using System.Collections.Generic;

namespace Cross.AppService.DTOs
{
    public class ExerciseDictionaryDTO
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string ExerciseName { get; set; }

    }


    public class ExerciseDictionarySettingsDTO
    {
        public int Id { get; set; }

        public int ExerciseDictionaryId { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public string Measure { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }
    }

    public class ExerciseDictionaryWithSettingsDTO : ExerciseDictionaryDTO
    {
        public ExerciseDictionaryWithSettingsDTO()
        {
            this.ExerciseSettings = new List<ExerciseDictionarySettingsDTO>();
        }

        public List<ExerciseDictionarySettingsDTO> ExerciseSettings { get; set; }
    }


    public class ExerciseDTO
    {
        public ExerciseDTO()
        {
            this.CreatedDate = DateTime.UtcNow;
        }

        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Планируемое кол-во повторений
        /// </summary>
        public int? ExpectedCount { get; set; }

        /// <summary>
        /// Кол-во повторений по факту
        /// </summary>
        public int? ResultCount { get; set; }

        /// <summary>
        /// Вес планируемый
        /// </summary>
        public int? ExpectedWeight { get; set; }

        /// <summary>
        /// Вес по факту
        /// </summary>
        public int? ResultWeight { get; set; }

        /// <summary>
        /// Порядок
        /// </summary>
        public int? Order { get; set; }

        public int? ExerciseDictionaryId { get; set; }

        public int? RoundId { get; set; }

        public string ExerciseName
        {
            get => this.ExerciseDictionary != null ? this.ExerciseDictionary.ExerciseName : "";
            set { }
        }

        public ExerciseDictionaryDTO ExerciseDictionary { get; set; }

    }

}