﻿using System.Collections.Generic;

namespace Cross.AppService.DTOs
{
    public class RoundDTO
    {
        public RoundDTO()
        {
            this.Exercise = new List<ExerciseDTO>();
        }

        public int Id { get; set; }

        public int? WodId { get; set; }

        public List<ExerciseDTO> Exercise { get; set; }
    }
}