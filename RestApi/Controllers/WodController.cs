﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cross.AppService.DTOs;
using Cross.AppService.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace RestApi.Controllers
{
    [Route("api/[controller]")]
    public class WodController : Controller
    {
        private IWodAppService _wodAppService;

        public WodController(IWodAppService wodAppService)
        {
            _wodAppService = wodAppService;
        }


        // POST api/Wod/CreateNewWod
        [HttpPost("CreateNewWod")]
        public WodDTO CreateNewWod()
        {
            return _wodAppService.CreateNewWod();
        }

        [HttpGet("GetAllWods")]
        public async Task<IEnumerable<WodDTO>> GetAllWods()
        {
            return await _wodAppService.GetAllWods();
        }
    }
}