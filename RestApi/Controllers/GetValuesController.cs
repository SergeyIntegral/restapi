﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cross.AppService.DTOs;
using Cross.AppService.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace RestApi.Controllers
{
    [Route("api/[controller]")]
    public class GetValuesController : Controller
    {
        private IExerciseAppService _exerciseAppService;

        public GetValuesController(IExerciseAppService exerciseAppService)
        {
            _exerciseAppService = exerciseAppService;
        }

        [HttpGet]
       /* [Route("api/[controller]/GetRootMembers")]*/
        public async Task<IEnumerable<ExerciseDictionaryWithSettingsDTO>> GetRootMembers()
        {
            return await _exerciseAppService.GetRootsWithSettings(); // _exerciseAppService.GetRootMembers();
        }

        [HttpGet("{parentId}")]
        public async Task<IEnumerable<ExerciseDictionaryWithSettingsDTO>> GetMembersByParentId(int parentId)
        {
            return await _exerciseAppService.GetMembersByParentIdWithSettings(parentId);
        }

        // /api/GetValues/GetAllExercisesByRoundId/{roundId}
        [HttpGet("GetAllExercisesByRoundId/{roundId}")]
        public async Task<IEnumerable<ExerciseDTO>> GetAllExercisesByRoundId(int roundId)
        {
            return await _exerciseAppService.GetAllExercisesByRoundId(roundId);
        }
        //api/GetValues/InsertNewExerciseWithRoundId/{roundId}
        [HttpPost("InsertNewExerciseWithRoundId/{roundId}")]
        public ExerciseDTO InsertNewExerciseWithRoundId(int roundId)
        {
            return _exerciseAppService.InsertNewExerciseWithRoundId(roundId);

        }

        // /api/GetValues/InsertNewExerciseWithRoundIdAsync/{roundId}
        [HttpPost("InsertNewExerciseWithRoundIdAsync/{roundId}")]
        public async Task<ExerciseDTO> InsertNewExerciseWithRoundIdAsync(int roundId)
        {
            return await _exerciseAppService.AddExerciseInsertNewExerciseWithRoundIdAsync(roundId);

        }



        // /api/GetValues/GetExerciseById/{id}
        [HttpGet("GetExerciseById/{id}")]
        public async Task<ExerciseDTO> GetExerciseById(int id)
        {
            return await _exerciseAppService.GetById(id);
        }


        [HttpGet("GetSettings/{id}")]
        public async Task<IEnumerable<ExerciseDictionarySettingsDTO>> GetSettings(int id)
        {
            var result = await _exerciseAppService.GetSettings(id);
            return result;
        }

    }
}