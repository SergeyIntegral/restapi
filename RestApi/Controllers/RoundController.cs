﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cross.AppService.DTOs;
using Cross.AppService.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace RestApi.Controllers
{
    [Route("api/[controller]")]
    public class RoundController : Controller
    {
        private readonly IRoundAppService _roundAppService;

        public RoundController(IRoundAppService roundAppService)
        {
            _roundAppService = roundAppService;
        }

        // POST api/Round/CreateNewRound/{id}
        [HttpPost("CreateNewRound/{id}")]
        public RoundDTO CreateNewRound(int id)
        {
            return _roundAppService.AddNewRound(id);
        }

        // GET api/Round/GetRoundsByWodId/{id}
        [HttpGet("GetRoundsByWodId/{id}")]
        public async Task<IEnumerable<RoundDTO>> GetRoundsByWodId(int id)
        {
            return await _roundAppService.GetRoundsByWodId(id);
        }

        // POST api/Round/CopyRound/{roundId}
        [HttpPost("CopyRound/{roundId}")]
        public async Task<RoundDTO> CopyRound(int roundId)
        {
            return await _roundAppService.CopyRound(roundId);
        }
    }
}