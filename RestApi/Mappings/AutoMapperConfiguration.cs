﻿namespace RestApi.Mappings
{
    public static class AutoMapperConfiguration
    {

        public static void Initialize()
        {
            AutoMapper.Mapper.Initialize((cfg) =>
            {
                cfg.AddProfiles(Cross.IoC.AutoMapperConfiguration.GetAutoMapperProfiles());
            });
        }
    }
}