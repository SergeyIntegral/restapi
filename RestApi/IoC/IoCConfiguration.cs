﻿using Cross.IoC;
using Microsoft.Extensions.DependencyInjection;

namespace RestApi.IoC
{
    public class IoCConfiguration
    {
        public static void Configure(IServiceCollection services)
        {
            foreach (var type in IoC.Module.GetSingleTypes())
                services.AddScoped(type);
            Bootstraper.Configure(services);
        }
    }
}