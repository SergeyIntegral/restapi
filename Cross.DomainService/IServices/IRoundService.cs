﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cross.Model;

namespace Cross.DomainService.IServices
{
    public interface IRoundService
    {
        Round AddNewRound(int wodId);

        Task <IQueryable<Round>> GetRoundsByWodId(int wodId);
        Task<Round> CopyRound(int roundId);
    }
}