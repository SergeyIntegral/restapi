﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cross.Model;

namespace Cross.DomainService.IServices
{
    public interface IExerciseDictionaryService
    {
        void Delete(int id);

        void Delete(ExerciseDictionary entity);

        ExerciseDictionary FindById(int id);

        Task<ExerciseDictionary> FindByIdAsync(int id);

        IEnumerable<ExerciseDictionary> FindByIds(IEnumerable<int> ids);

        Task<IEnumerable<ExerciseDictionary>> FindByIdsAsync(IEnumerable<int> ids);

        IEnumerable<ExerciseDictionary> GetAll();

        Task<IEnumerable<ExerciseDictionary>> GetAllAsync();

        ExerciseDictionary Insert(ExerciseDictionary entity);

        ExerciseDictionary Update(ExerciseDictionary entity);

        Task<IQueryable<ExerciseDictionary>> GetRootMembers();

        Task<IQueryable<ExerciseDictionary>> GetMembersByParentId(int parentId);

        Task<IQueryable<ExerciseExtendedSettings>> GetExerciseDictionarySettings();

        Task<IQueryable<ExerciseExtendedSettings>> GetExerciseDictionarySettingsByExerciseDictionaryId(int ExerciseDictionaryId);

        Task<IQueryable<Exercise>> GetAllExercisesByRoundId(int roundId);

        Exercise InsertNewExerciseWithRoundId(int roundId);

        Task<Exercise> InsertNewExerciseWithRoundIdAsync(int roundId);

        void SaveChanges();

        Task SaveChangesAsync();

        Task<Exercise> GetById(int id);

        Task<IQueryable<ExerciseExtendedSettings>> GetExerciseDictionarySettingsByExercyseId(int exerciseID);

    }
}