﻿using System.Linq;
using System.Threading.Tasks;
using Cross.Model;

namespace Cross.DomainService.IServices
{
    public interface IWodService
    {
        Wod CreateNewWod();

        Task<IQueryable<Wod>> GetAllWods();
    }
}