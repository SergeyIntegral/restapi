﻿using System;
using System.Collections.Generic;
using Cross.DomainService.IServices;
using Cross.DomainService.Service;

namespace Cross.DomainService.IoC
{
    public static class Module
    {
        public static Dictionary<Type, Type> GetTypes()
        {
            var dic = new Dictionary<Type, Type>();
            dic.Add(typeof(IExerciseDictionaryService), typeof(ExerciseService));
            dic.Add(typeof(IRoundService), typeof(RoundService));
            dic.Add(typeof(IWodService), typeof(WodService));
            return dic;
        }
    }
}