﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cross.DAL;
using Cross.DomainService.IServices;
using Cross.Model;
using Microsoft.EntityFrameworkCore;

namespace Cross.DomainService.Service
{
    public class ExerciseService : IExerciseDictionaryService
    {
        private readonly IExerciseDictionaryObjectRepository _exerciseDictionaryRepository;
        private readonly IExerciseSettingRepository _exerciseSettingRepository;
        private readonly IExerciseObjectRepository _exerciseObjectRepository;
        private IUnitOfWork _uow;

        public ExerciseService(IExerciseObjectRepository exerciseObjectRepository,
            IExerciseDictionaryObjectRepository exerciseDictionary,
            IExerciseSettingRepository exerciseSettingRepository,
            IUnitOfWork uow)
        {
            _uow = uow;
            _exerciseObjectRepository = exerciseObjectRepository;
            _exerciseDictionaryRepository = exerciseDictionary;
            _exerciseSettingRepository = exerciseSettingRepository;
        }

        public void Delete(int id)
        {
            _exerciseDictionaryRepository.Delete(id);
        }

        public void Delete(ExerciseDictionary entity)
        {
            _exerciseDictionaryRepository.Delete(entity);
        }

        public ExerciseDictionary FindById(int id)
        {
            return _exerciseDictionaryRepository.FindById(id);
        }

        public async Task<ExerciseDictionary> FindByIdAsync(int id)
        {
            return await _exerciseDictionaryRepository.FindByIdAsync(id);
        }

        public IEnumerable<ExerciseDictionary> FindByIds(IEnumerable<int> ids)
        {
            return _exerciseDictionaryRepository.FindByIds(ids);
        }

        public async Task<IEnumerable<ExerciseDictionary>> FindByIdsAsync(IEnumerable<int> ids)
        {
            return await _exerciseDictionaryRepository.FindByIdsAsync(ids);
        }

        public IEnumerable<ExerciseDictionary> GetAll()
        {
            return _exerciseDictionaryRepository.GetAll();
        }

        public async Task<IEnumerable<ExerciseDictionary>> GetAllAsync()
        {
            return await _exerciseDictionaryRepository.GetAllAsync();
        }

        public ExerciseDictionary Insert(ExerciseDictionary entity)
        {
            return _exerciseDictionaryRepository.Insert(entity);
        }

        public ExerciseDictionary Update(ExerciseDictionary entity)
        {
            return _exerciseDictionaryRepository.Update(entity);
        }

        public async Task<IQueryable<ExerciseDictionary>> GetRootMembers()
        {
            return await Task.Run(() =>
            {
                return _exerciseDictionaryRepository.AsQueryable().Where(e => e.ParentId == null);
            });
        }

        public async Task<IQueryable<ExerciseDictionary>> GetMembersByParentId(int parentId)
        {
            return await Task.Run(() =>
            {
                return _exerciseDictionaryRepository.AsQueryable().Where(e => e.ParentId == parentId);
            });
        }

        public async Task<IQueryable<ExerciseExtendedSettings>> GetExerciseDictionarySettings()
        {
            return await Task.Run(() => _exerciseSettingRepository.AsQueryable());
        }

        public async Task<IQueryable<ExerciseExtendedSettings>> GetExerciseDictionarySettingsByExercyseId(int exerciseID)
        {
            return await Task.Run(() => _exerciseSettingRepository.AsQueryable().Where(x=>x.ExerciseDictionaryId == exerciseID));
        }

        public async Task<IQueryable<ExerciseExtendedSettings>> GetExerciseDictionarySettingsByExerciseDictionaryId(int exerciseId)
        {
            return await Task.Run(() =>
            {
                return _exerciseSettingRepository.AsQueryable().Where(x => x.ExerciseDictionaryId == exerciseId);
            });
        }

        public async Task<IQueryable<Exercise>> GetAllExercisesByRoundId(int roundId)
        {
            return await Task.Run(() =>
            {
                return _exerciseObjectRepository.AsQueryable().Where(x => x.RoundId.HasValue && x.RoundId.Value == roundId).Include(a=>a.ExerciseDictionary);
            });
        }

        public Exercise InsertNewExerciseWithRoundId(int roundId)
        {
            var exercise = new Exercise();
            exercise.RoundId = roundId;
            var insertedExercise = _exerciseObjectRepository.Insert(exercise);
            SaveChanges();
            return _exerciseObjectRepository.FindById(exercise.Id);
        }

        public async Task<Exercise> InsertNewExerciseWithRoundIdAsync(int roundId)
        {
            var exercise = new Exercise();
            exercise.RoundId = roundId;
            var insertedExercise = _exerciseObjectRepository.Insert(exercise);
            return await SaveChangesAsync().ContinueWith(e=>
            {
                return _exerciseObjectRepository.FindByIdAsync(exercise.Id);
            }).Unwrap();
        }


        public async Task<Exercise> GetById(int id)
        {
            return await _exerciseObjectRepository.FindByIdAsync(id);
        }


        public void SaveChanges()
        {
            _uow.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _uow.SaveChangesAsync();
        }
    }
}