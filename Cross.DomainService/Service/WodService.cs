﻿using System.Linq;
using System.Threading.Tasks;
using Cross.DAL;
using Cross.DomainService.IServices;
using Cross.Model;

namespace Cross.DomainService.Service
{
    public class WodService : IWodService
    {
        private readonly IUnitOfWork _uow;
        private readonly IWodObjectRepository _wodObjectRepository;

        public WodService(IUnitOfWork uow, IWodObjectRepository wodObjectRepository)
        {
            _uow = uow;
            _wodObjectRepository = wodObjectRepository;
        }

        public Wod CreateNewWod()
        {
            var wod = new Wod();
            var insertedWod = _wodObjectRepository.Insert(wod);
            _uow.SaveChanges();
            var result =_wodObjectRepository.FindById(wod.Id);
            return result;

        }

        public async Task<IQueryable<Wod>> GetAllWods()
        {
            return  await Task.Run(()=> _wodObjectRepository.AsQueryable());
        }
    }
}