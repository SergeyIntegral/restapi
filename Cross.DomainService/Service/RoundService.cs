﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cross.DAL;
using Cross.DomainService.IServices;
using Cross.Model;
using Microsoft.EntityFrameworkCore;

namespace Cross.DomainService.Service
{
    public class RoundService : IRoundService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRoundObjectRepository _roundObjectRepository;
        private readonly IExerciseObjectRepository _exerciseObjectRepository;

        public RoundService(IUnitOfWork uow, IRoundObjectRepository roundObjectRepository,
            IExerciseObjectRepository exerciseObjectRepository)
        {
            _uow = uow;
            _roundObjectRepository = roundObjectRepository;
            _exerciseObjectRepository = exerciseObjectRepository;
        }

        public Round AddNewRound(int wodId)
        {
            var round = new Round(wodId);
            var insertedRound = _roundObjectRepository.Insert(round);
            _uow.SaveChanges();
            return _roundObjectRepository.FindById(round.Id);
        }

        public async Task<IQueryable<Round>> GetRoundsByWodId(int wodId)
        {
            return await Task.Run(() =>
            {
                return _roundObjectRepository.AsQueryable().Where(r => r.WodId != null && r.WodId.Value == wodId).Include(w=>w.Exercise).Include(a=>a.Wod);
            });
        }

        public async Task<Round> CopyRound(int roundId)
        {
            var existRound = _roundObjectRepository.FindById(roundId);
            return await copyRound(existRound);
        }

        private async Task<Round> copyRound(Round round)
        {
            var newRound = new Round
            {
                Exercise = null,
                WodId = round.WodId,
                Wod = round.Wod
            };
            _roundObjectRepository.Insert(newRound);
            _uow.SaveChanges();

            return round.Exercise != null
                ? await CopyExercises(round.Exercise, newRound.Id).ContinueWith(e =>
                {
                    return _roundObjectRepository.FindByIdAsync(newRound.Id);
                }).Unwrap()
                : await _roundObjectRepository.FindByIdAsync(newRound.Id);
        }

        private async Task<ICollection<Exercise>> CopyExercises(ICollection<Exercise> roundExercise, int roundId)
        {
            return await Task.Run(() =>
            {
                var result = new List<Exercise>();
                foreach (var item in roundExercise)
                {
                    var newExercise = new Exercise
                    {
                        CreatedDate = DateTime.UtcNow,
                        ExerciseDictionary = item.ExerciseDictionary,
                        ExerciseDictionaryId = item.ExerciseDictionaryId,
                        ExpectedCount = item.ExpectedCount,
                        ModifiedDate = item.ModifiedDate,
                        Order = item.Order,
                        ExpectedWeight = item.ExpectedWeight,
                        ResultCount = item.ResultCount,
                        ResultWeight = item.ResultWeight,
                        RoundId = roundId
                    };
                    var copyExercise = _exerciseObjectRepository.Insert(newExercise);
                    _uow.SaveChanges();
                    result.Add(copyExercise);
                }

                return result;
            });
        }
    }
}